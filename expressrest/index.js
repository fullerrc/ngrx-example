const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');

const config = require('./config');
const itemsRouter = require('./domains/items');

const { nodeHost, nodePort, nodeProtocol } = config.node;

app.use(cors());
app.use(bodyParser.json());

app.use('/items', itemsRouter);

app.listen(nodePort, () => {
 console.log(`Server running on port ${nodeProtocol}://${nodeHost}:${nodePort}/`);
});