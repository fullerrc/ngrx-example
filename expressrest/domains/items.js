const express = require('express');
const router = express.Router();
var data = require('./data.json');

/* GET. */
router.get('/', function(req, res) {
    res.json(data);
});

/* POST. */
router.post('/', function(req, res) {
    data.push(req.body);
    res.status(200).json(req.body);
});

/* PUT. */
router.put('/:name', function(req, res) {
    data = data.map(datum => datum.name === req.body.name ? req.body : datum);
    res.status(200).json(req.body);
});

/* DELETE. */
router.delete('/:name', function(req, res) {
    data = data.filter(datum => datum.name !== name);
    res.status(200).json({ message: "Item 'name' removed."});
});

module.exports = router;