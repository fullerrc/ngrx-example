import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ItemListComponent } from './item-list/item-list.component';
import { BackendService } from './services/backend.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreEffects } from './store/store.effects';
import { HttpClientModule } from '@angular/common/http';
import { storeReducer } from './store/store.reducers';

@NgModule({
  declarations: [
    AppComponent,
    ItemListComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({ list: storeReducer }),
    EffectsModule.forRoot([StoreEffects]),
  ],
  providers: [BackendService],
  bootstrap: [AppComponent]
})
export class AppModule { }
