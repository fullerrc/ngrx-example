import { Injectable } from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { IItem } from '../store/store.interfaces';

@Injectable()
export class BackendService {
    apiUrl = 'http://localhost:8080/items';

    constructor (private http: HttpClient) {}

    fetchList () {
        return this.http.get<IItem[]>(this.apiUrl);
    }
    addItem (item: IItem) {
        return this.http.post<IItem[]>(this.apiUrl, item);
    }
    updateItem (item: IItem) {
        return this.http.put<IItem[]>(this.apiUrl, item);
    }
}