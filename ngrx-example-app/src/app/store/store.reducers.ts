import { StoreActions } from './store.actions';
import * as StoreActionTypes from './store.types';
import { IItem } from './store.interfaces';

export interface StoreState {
    data: IItem[];
};

export const storeInitialState = {
    data: [],
};

export function storeReducer (state: StoreState = storeInitialState, action: StoreActions) {
    switch (action.type) {
        case (StoreActionTypes.FetchListSuccess): {
            return {
                ...state,
                data: action['payload'],
            }
        }
        case (StoreActionTypes.AddItemSuccess): {
            return {
                ...state,
                data: [...state.data, action['payload']],
            };
        }
        // case (StoreActionTypes.UpdateItemSuccess): {
        //     return {
        //         ...state,
        //         data: state.data.map((item: IItem) => item.name === action['payload'].name ? action['payload'] : item),
        //     };
        // }
        default: {
            return {
                ...state,
            };
        }
    }
}