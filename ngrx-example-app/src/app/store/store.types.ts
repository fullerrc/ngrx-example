export const FetchList = 'FetchList';
export const FetchListSuccess = 'FetchListSuccess';
export const FetchListError = 'FetchListError';

export const AddItem = 'AddItem';
export const AddItemSuccess = 'AddItemSuccess';
export const AddItemError = 'AddItemError';

export const UpdateItem = 'UpdateItem';
export const UpdateItemSuccess = 'UpdateItemSuccess';
export const UpdateItemError = 'UpdateItemError';
