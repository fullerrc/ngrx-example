export interface IItem {
    name: string;
    quantity: number;
    foobar: boolean;
}
