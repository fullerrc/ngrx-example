import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import * as StoreActionTypes from './store.types';
import * as StoreActions from './store.actions';
import { BackendService } from '../services/backend.service';
import { IItem } from './store.interfaces';

@Injectable()
export class StoreEffects {
    constructor (private actions$: Actions, private backendService: BackendService) {}
    @Effect()
    fetchList = this.actions$
        .ofType(StoreActionTypes.FetchList)
        .pipe(
            switchMap((action: StoreActions.FetchList) => this.backendService.fetchList()),
            map((response: IItem[]) => new StoreActions.FetchListSuccess(response)),
        );
}