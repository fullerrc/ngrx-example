import * as StoreActionTypes from './store.types';
import { Action } from '@ngrx/store';
import { IItem } from './store.interfaces';

export class FetchList implements Action {
    readonly type = StoreActionTypes.FetchList;
    // No constructor needed -- there's only one list to fetch.
}
export class FetchListSuccess implements Action {
    readonly type = StoreActionTypes.FetchListSuccess;
    constructor (public payload: IItem[]) {}
}

export class AddItem implements Action {
    readonly type = StoreActionTypes.AddItem;
    // Constructor here, since triggering adding an item will need to carry
    // the item to be added.
    constructor (public payload: IItem) {}
}
export class AddItemSuccess implements Action {
    readonly type = StoreActionTypes.AddItemSuccess;
    // Payload will carry the response from the API, which we will set
    // to be the item that was just created.
    constructor (public payload: IItem[]) {}
}

export class UpdateItem implements Action {
    readonly type = StoreActionTypes.UpdateItem;
    // Constructor here, since triggering Updating an item will need to carry
    // the item to be Updated.
    constructor (public payload: IItem) {}
}
export class UpdateItemSuccess implements Action {
    readonly type = StoreActionTypes.UpdateItemSuccess;
    // Payload will carry the response from the API, which we will set
    // to be the item that was just created.
    constructor (public payload: IItem[]) {}
}


export type StoreActions = 
    | FetchList 
    | FetchListSuccess 
    | AddItem 
    | AddItemSuccess 
    | UpdateItem 
    | UpdateItemSuccess ;
