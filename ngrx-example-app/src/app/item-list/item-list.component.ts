import { Component, OnInit } from '@angular/core';
import { IItem } from '../store/store.interfaces';
import { Store } from '@ngrx/store';

import * as fromStore from '../store/store.reducers';
import { Observable } from 'rxjs';
import { FetchList } from '../store/store.actions';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  itemList: IItem[] = [];
  itemListState$: Observable<fromStore.StoreState>;

  constructor(private store: Store<fromStore.StoreState>) {}

  ngOnInit() {
    // this.itemList = [
    //   {
    //     name: 'First Item',
    //     quantity: 3,
    //     foobar: false,
    //   },
    //   {
    //     name: 'Second Item',
    //     quantity: 23,
    //     foobar: false,
    //   }
    // ];
    this.itemListState$ = this.store.select('list');
    // this.itemListState$.subscribe((foo: fromStore.StoreState) => {
    //   console.log(foo.data);
    //   this.itemList = foo.data;
    // });
    this.store.dispatch(new FetchList());
  }

}
